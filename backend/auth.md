# Authentication, authorization and accounts

## creating new account

* either
  * buy once account token for 25c to play, enter desired identity, no login name
  * each identity change for 25c
  * tokens will be files generated once and stored on the device that will expire after a time - they will be basically the public keys presented by the device to be downloaded
    * in the handshake the device will also respond with it's own public key that will then become part of the private key of the device - it will encrypt it with it's own private key to present its identity, the server will decrypt it using the public key and see that it matches the private key assigned to that player
    
  * ? or by linking with facebook/google/twitter/instagram etc (later)
  * ? directly from the client by clicking on the offline red icon (only able to browse, look in past), but not interact
