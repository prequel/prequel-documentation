# backend design choices


* node.js, express, containers
* maximal modularization, no script larger than 150 lines


## unsorted


* server side data field is stored as:
  * an buffer/array of colors to download - current time
  * timeslices of operations by individual players to be able to reconstruct the field at any past point of time
  * stored timeslice plans of players validated against current field, can be resulting in invalid operations (client side planning is just estimate/linear extrapolation from current trends )
* when a client requests a timeslice from the past, he'll get stream current field and all timeslices to that point in the past to client-side reconstruct the field's state since then
  * ? depends if the field will be too large, only stream just relevant area

* timestamp will be in the format: YYYYMMDD_hhmm

* player data (configuration) - skills values, stats

* store snapshots of current fields as TGA's after each processing step
  
* server will have a 'static' array of entries which player operated on which pixel so that they can be updated during evaluation without having to keep iterating over all the players - i.e. field [X,Y] is 'owned' by players 33,45,75 so when player 89 uses a skill, only process those 3 player's claims on that pixel


* maximum number of players inside one pixel is 10

* !!! - two primary colors do not diminish energy gain, three do based on ctor - zero saturation


* !!! the pos_y will start in 0 and grow to both sides symmetrically; this way the growth and shrinking won't introduce complicated recalculation of Y positions when shrinking/growing

* each action stored as separate json?

processing a single update/heartbeat
=====================

1. go through all queued actions for each player and (if no actions planned, treat player as idling and just accumulating energy):
   1. update in db that this action was processed.. (?move it to another collection/db)
   1. if the player has enough energy, apply this action on the field in it's current pristine state, calculate the delta for each claim of each component add the delta to the current delta for that field's components and their claim owners in the server's game state memory
   1. if the player does not have enough energy, mark this action as failed
1. at the end, go through all the fields and apply their deltas, capped by [0..255] for each RGB component
  * recalculate the ownership percentages in this step somehow !!!TODO
? claim ownership delta - if this skill goes through, how would the ownerships for each component change - skrink existing 
* note! action can fizzle - decresing zero will result in zero but energy will still be consumed, the same with increasing over max

!!! not planning action will be considered idling 
   
  
claims
===
* current claims kept in memory for the purpose of updating the field during processing
* stored after each processing step with the players so that each player can look through his when browsing his kingdom - store only current claims for the purpose of restoring the field claims in memory of the server
* ? store previous claims also so that player can reconstruct his own history?
