# player

* player will have his own record/tracking of contributions to pixels
  * this will be stored server side
  * this will be used for the purposes of skills
  * when 'stealing' from other player's pixels, the reduction will be proportional for all - i.e. reducing full red by one third that is owned 90% by player A and 10% by player B, player A will have 60  and player B 7

* player can show an overlay showing pixels he has affected and his current zone of influence
* during day he'll receive energy from all his pixels, during night only the ones in his zone will retain his claims, rest will slowly decay
* decay rate should be exponential - slow at start, increasing in speed, exact rate has to be figured out

## frontend

* player stats
* history of moves
* prediction of energy levels
* claims view (switch)