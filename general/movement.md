# Movement

* movement is not considered a skill in terms of chaos/order, the skill of movement can be affected by CTOR - for example pure chaos can have a random element that can increase or decrease direction and speed of movement
* ? movement can be a function of distance and effectively a jump to a specified pixel with calculated energy cost - equal to issuing multiple orders - select a pixel to move to and calculate energy cost of such move, then based on CTOR either discount it or randomize it:
  * ctor 1 - movement inside order space (white pixels) is discounted by upto 75% based on how low saturation is in the pixels (1,1,1 will count the same as 255,255,255) that will be traversed by the move, and equally multiplied if outside of order space (up to 400% more expensive)
  * ctor 0.5 - discount upto 50% when moving over pixels with 100% my tribe hue and maximal saturation, 200% more expensive cost outside
  * ctor 0 - no discounts or penalties but increasing randomness - mostly distance, slightly direction, occasionally tunnel/teleport significantly