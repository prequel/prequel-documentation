# General ideas

## design choices

* focused on playing it on a mobile device
* controlable with touch on mobile
* ability to queue commands
* prediction estimate of accumulated power levels if nobody interfered so that players can know which actions can be actually planned
* minimal ui - use tabs for different views
* ? marching ants indicator of claimed pixels?
* ? fog of war
* map is a strip of land that has a day-night cycle from west to east that expands to north and south based on how many people were playing the last day, the border will show in a green/red overlay by how much it will expand or contract

* ? just two zoom levels - one for interacting with approx 30x30 pixel size, preview one 1 pixel on screen equal to 1 pixel playing field, screen size shown as a inverse color bounding box  or a white 1px border bounding box with extra black 1px border
  * maybe implement the inversion in a shader or smth

* resources of a player should be based on his approach - balanced player can be both tolerant/cooperative but also isolationist/individualist
* if the player will have tendency towards one, options should open to reinforce that trend
* balancing should be done based on average energy income per turn for fully commited players - it should be roughly the same, so that overall power does not skewer in one direction

## day/night cycle affected by _CTOR_

* affecting fog of war
* player locations will be revealed
* drastically lower efficiency during night based on real timezones to promote grouping by time zones - pixels not in _zone of influence_ during night decay
* ? during day all pixels will be visible, OR during day the fog of war will be less restricted - i.e. visibility something like 2/3x radius of the sphere of influence
  * CTOR affecting this - during nighttime, chaos players will have undiminished visibility within their sphere of influence, order will be basically blind - way smaller visibility than even their sphere of influence

* !!! vertical bands of 'light and darkness' based on timezone that will rotate and cause a day-night cycle
* !!! map will wrap on itself anyway since it will be a dynamically scaling playfield, but only in one axis (width), vertical playfield will be constant to not mess with the timezones
* chaos players will be less exposed to it - suffer lesses penalties during night but less bonuses during day
* order players will be the exact opposite - severe punisment during night but significant bonuses during day
* horizontal center will be most 'stable' but most competed for, edges will be risky but full of oportunity for expansion

* expansion/shrinking of the 'mirror barrier' will be each day at 0:00 GMT, once per day

## introduction/tutorial

* fog of war
* 'choose your tribe'
* claim first pixel(s)
* accumulate energy
* show past slices
* plan actions ahead
* see results of the plan
* simple game, complex gameplay

## multiple players planning action against the same pixel at one time

* the calucaltion will go through on the pixel as if only one player was affecting it individually, the result will be weighted averaged and the effectiveness adjusted, energy consumed will be the same
  * this means multiple players operating on one pixel diminishes effectiveness and multiple players can't speed up the process of moving the pixel's colors in one direction

## questionable - to decide

* ? allow skills to be used in multiple ways? - i.e. burst vs sustain etc
* ? register players by nickname/show online players?
* ? clans?
* ? short seasons (few weeks to a 1-2 months?) to test out new game mechanics? do not separate community unless needed, maybe have a legacy and league game worlds - permanent one with a temporary one
* ? accumulate only achievements
* ? public list of unknown achievements like the tome of knowledge
* ? easter eggs/secrets?
* ? cluster of pixels live on their own - after reaching a certain size, sustain themselves, if grown too large, produce trash, decay into random/black