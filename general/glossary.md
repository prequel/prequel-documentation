# Glossary

## Zone of influence (ZOI)

* circular zone of influence with a radius 
* radius and thus potential accumlated derived power will grow over time
* commands issued further away from the center will have higher power cost - this should promote actually moving to the clusters to maximize efficiency

## ? Cluster

* ? having several pixels clumped up in a cluster will slow down or even stop decay when not affecting them by the zone
* clusers will decay from the outside in - i.e. pixel that has claims from all 4 surrounding sides forming a cross will have those on the outside diminish first before affecting the inner one
* the larger the cluster, the more protected pixels inside of it

## Pixel

## CTOR

* chaos to order ratio for the player.. 0 is pure chaos, 0.5 perfect balance, 1 pure order

## Claim

* applying a skill on a pixel will usually increase one or more of the primary colors in that pixel, this increase is tied to the claim of the player using the skill

## Energy

* players gradually get energy from their claims and based on their CTOR their faction also, more in [energy.md](energy.md)

## Skills

* players can use skills by spending energy, more in [skills.md](skills.md)

## Movement

* players can spend energy to move, special rules can apply based on CTOR, more in [movement.md](movement.md)

## Collective energy pool

* non-accumulating
* aimed to help players who are low on energy and need to atleast move to some place to start generating their own supply
* Individual tribes (R, G and B tribes) as well as Order faction will provide active players a 'wealth transfer' share of the cumulative energy from all pixels's individual color channels
* for this energy is sumed across all pixels and divided evenly among all players based on their CTOR.
  * players with ctor 0.5 will get full share of their tribe's collective energy