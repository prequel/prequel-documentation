# Skills

* game is starting with 3 skills, one moving slightly towards chaos, one towards balance and one neutral
  * opening up will be 2 skills in each opposite direction to a total of 7 skills available
  * depending on the current CTOR, the skills will be moving towards their original place.. i.e. for someone directly in the middle, using the slightly chaos skill will move them towards chaos, but if that player is pure chaos, that skill will move them towards balance
* only 3 of those available at any one time based on CTOR

> strong chaos: lower contribution from all 3 colors of other's contribution and increase random mine (r or g or b)  - (?chaos player can result in having a pure white just his own effectively hiding inside order space?)
> medium chaos: replace part of enemy color by my contrib and increase slightly one random color (if i'm red, reduce green or blue and increase )
> light chaos: lower one random enemy color and increase mine (if im red, reduce green or blue to increase red)
> pure balance: lower both enemy colors and increase mine
> light order: increase my tribe color in a pixel, reduce the lower of the two remaining colors
> medium order: increase my tribe color in a pixel, keep the other two intact OR lower the higher one
> strong order: increase the lowest color
