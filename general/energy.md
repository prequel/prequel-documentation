* balance(tribes) and order have a common energy pool that is redistributed to ppl equally based on their adherence to the tribe/order
* when going chaos, this effect diminishes towards zero, but energy from individualy owned pixels is amplified
* energy comes from the claims in pixels all over the map, not necessarily inside the ZOI
* unspent energy can accumulate infinitely
* collective energy is always spent first (CTOR 0 have no collective energy)