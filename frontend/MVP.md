MVP
===

* 1 ip adress 1 user
* upon connecting claims a random pixel and itself becomes r or g or b
* playfield is 10*10 pixels 
* can zoom in and out and scroll when zoomed , click to unlock sidebar like in google maps (react?)
* can issue order to increse his color in a pixel once per 5 min, no queue
* has 2 max energy that regenerates once per 5 min

* ? with each request return token for a new request
! reinventing the wheel with authorization, look it up

! APIs are to be visible for the client, not internal game server state-db requests

antiflooding [overthinking it? - the client can have protection agains this instead]
* each user will have a limit on how many requests he can make in a certain time period, it will only get incremented for each request and checked at the end of the time period;
* only check users that made requests so the whole table does not need to be rechecked every time
* period one minute?
* if a user surpassed his quota, autocheck this two more times, if so temporarily disable the ability to make requests, autoenable it after some time period (5 minutes?) if it happens again, increase this period to 1 hour, then day, eventually silently freeze/ghost account - autodrop requests other than user management/contact support?
