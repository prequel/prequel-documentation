* when scrolling and reaching borders top/down/left/right, the camera will 'teleport' to a position so that it can look like it's continuous scrolling
  * this means that the ui needs to show atleast one replica beyond each of the borders, so instead of 'rendering' one field, we'll have to render 9 of them (3x3)
* max zoom out level = 1 pixel data =1pixel on screen, max zoom in approx 30x30 screen pixels for one pixel of data, maybe more depending on minimal square size on a device

* !  brightness of pixels at night will be based on accumulated energy multiplied by that player's power (energy gain/turn)
* ? have skill multiplier even??!?
* ? maybe in the beginning allow only a few skill uses and force players to rest between moves